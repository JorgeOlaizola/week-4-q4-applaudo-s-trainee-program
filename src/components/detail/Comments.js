import React, { useEffect, useState } from 'react';
import { commentsHandler } from '../../adapters/fetch';
import Comment from './Comment';

export default function Comments({ id }) {
  const [comments, setComments] = useState([]);
  useEffect(() => {
    commentsHandler(id, 'GET')
      .then((response) => response.json())
      .then((data) => setComments(data))
      .catch((error) => console.log(error));
  }, []);

  return (
    <div className='comments-container'>
      {comments && comments.length > 0 ? (
        comments.map(({ id, user, comment }) => <Comment key={id} user={user} comment={comment}></Comment>)
      ) : (
        <div className='comment'>No comments yet. Be the first one!</div>
      )}
    </div>
  );
}
