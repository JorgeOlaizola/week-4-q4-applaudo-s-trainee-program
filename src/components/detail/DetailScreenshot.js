import React from 'react';

export default function DetailScreenshot({ screenshots }) {
  screenshots = screenshots.slice(0, 4);
  return (
    <div className='detail-screenshot-container'>
      {screenshots &&
        screenshots.length > 0 &&
        screenshots.map(({ image, id }) => <img key={id} className='detail-screenshot' src={image} alt={id} />)}
    </div>
  );
}
