import React from 'react';
import '../../styles/detail.scss';

export default function DetailInfo({ name, image, description, genres, platforms, rating, released }) {
  return (
    <div className='detail-info-container'>
      <h2>{name}</h2>
      <div className='detail-info-container-separator'>
        <div className='detail-info-image'>
          <img className='detail-image' src={image} alt={name} />
        </div>
        <div className='detail-info-text'>
          <h4>Description</h4>
          {description}
          <h4>Genres</h4>
          <div className='detail-info-genres'>
            {genres && genres.length > 0 && genres.map((genre) => <span key={genre.id}>{genre.name + ' '}</span>)}
          </div>
          <h4>Platforms</h4>
          <div>
            {platforms &&
              platforms.length > 0 &&
              platforms.map(({ platform }) => <span key={platform.id}>{platform.name + ' '}</span>)}
          </div>
          <h4>Rating</h4>
          <div>{'⭐'.repeat(Math.round(rating)) + ` (${rating})`}</div>
          <h4>Released</h4>
          <div>{released}</div>
        </div>
      </div>
    </div>
  );
}
