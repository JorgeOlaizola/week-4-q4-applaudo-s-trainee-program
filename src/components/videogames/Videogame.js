import React from 'react';

export default function Videogame({ props, changePage }) {
  const { name, background_image: image, id } = props;
  return (
    <div className='videogame'>
      <h1 className='videogame-title'>{name}</h1>
      <img className='videogame-image' src={image} alt={`${name}_${id}_img`}></img>
      <button onClick={() => changePage(id)} className='videogame-button'>
        Detail
      </button>
    </div>
  );
}
