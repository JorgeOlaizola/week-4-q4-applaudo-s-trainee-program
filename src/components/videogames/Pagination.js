import React from 'react';
import '../../styles/videogames.scss';

export default function Pagination({ videogamesPerPage, totalVideogames, paginate, currentPage }) {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalVideogames / videogamesPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <div className='pagination-container'>
      {pageNumbers.map((page) => {
        return (
          <button
            key={page}
            className={currentPage === page ? 'pagination-item-selected' : 'pagination-item'}
            onClick={() => paginate(page)}
          >
            {page}
          </button>
        );
      })}
    </div>
  );
}
