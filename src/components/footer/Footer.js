import React from 'react';
import '../../styles/footer.scss';

export default function Footer() {
  return (
    <footer className='footer'>
      <span className='footer-item'>Videogames app</span>
      <span className='footer-item'>Created by Jorge Olaizola</span>
      <span className='footer-item'>
        Follow me on{' '}
        <a href='https://gitlab.com/JorgeOlaizola' target='_blank'>
          GitLab
        </a>
      </span>
    </footer>
  );
}
