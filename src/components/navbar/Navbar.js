import React from 'react';
import '../../styles/navbar.scss';
import { pageIcon } from '../../adapters/globals';

export default function Navbar({ changePage }) {
  return (
    <nav>
      <img onClick={() => changePage('home')} className='icon' alt='page-icon' src={pageIcon} />
    </nav>
  );
}
