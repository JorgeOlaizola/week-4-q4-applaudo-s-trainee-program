import React, { useEffect, useState } from 'react';
import { fetchInfo } from '../adapters/fetch';
import '../styles/detail.scss';
import DetailInfo from '../components/detail/DetailInfo';
import DetailScreenshot from '../components/detail/DetailScreenshot';
import Comments from '../components/detail/Comments';

export default function Detail({ changePage, id }) {
  const [game, setGame] = useState({});
  const [screenshots, setScreenshots] = useState([]);

  useEffect(() => {
    fetchInfo(`/games/${id}`)
      .then((response) => response.json())
      .then((data) => {
        setGame(data);
        return fetchInfo(`/games/${id}/screenshots`);
      })
      .then((response) => response.json())
      .then((data) => setScreenshots(data.results))
      .catch((error) => console.log(error));
  }, []);

  const { name, description_raw: description, background_image: image, genres, platforms, rating, released } = game;
  return (
    <div className='detail-comments-container'>
      <div className='detail-container'>
        <button className='detail-back-button' onClick={() => changePage('home')}>
          ⬅ Go Back
        </button>
        <DetailInfo
          name={name}
          description={description}
          image={image}
          genres={genres}
          platforms={platforms}
          rating={rating}
          released={released}
        />
        <h3>Screenshots</h3>
        <DetailScreenshot screenshots={screenshots} />
      </div>
      <Comments id={id} />
    </div>
  );
}
