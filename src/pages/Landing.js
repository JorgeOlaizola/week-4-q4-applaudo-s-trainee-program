import React from 'react';
import { pageIcon } from '../adapters/globals';
import '../styles/landing.scss';

export default function Landing({ changePage }) {
  return (
    <div className='landing-container'>
      <h1 className='landing-title'>Welcome to the videogames App</h1>
      <img className='landing-logo' src={pageIcon} alt='logo-image' />
      <hr />
      <button className='landing-button' onClick={() => changePage('home')}>
        Get started!
      </button>
    </div>
  );
}
