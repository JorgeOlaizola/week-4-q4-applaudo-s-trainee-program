import React, { useState, useEffect } from 'react';
import { fetchInfo } from '../adapters/fetch';
import Pagination from '../components/videogames/Pagination';
import Videogames from '../components/videogames/Videogames';
import '../styles/home.scss';

export default function Home({ changePage }) {
  const [videogames, setVideogames] = useState([]);
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [videogamesPerPage, setPostsPerPage] = useState(8);
  useEffect(() => {
    setLoading(true);
    fetchInfo('/games', 'page_size=40')
      .then((response) => response.json())
      .then((data) => {
        setVideogames(data.results);
        setLoading(false);
      });
  }, []);

  function paginate(page) {
    setCurrentPage(page);
  }

  const indexOfLastVideogame = currentPage * videogamesPerPage;
  const indexOfFirstVideogame = indexOfLastVideogame - videogamesPerPage;
  const currentVideogame = videogames.slice(indexOfFirstVideogame, indexOfLastVideogame);
  return (
    <div className='home'>
      <Videogames videogames={currentVideogame} changePage={changePage} loading={loading} />
      <Pagination
        videogamesPerPage={videogamesPerPage}
        totalVideogames={videogames.length}
        paginate={paginate}
        currentPage={currentPage}
      ></Pagination>
    </div>
  );
}
