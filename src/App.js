import Landing from './pages/Landing';
import Home from './pages/Home';
import Navbar from './components/navbar/Navbar';
import Footer from './components/footer/Footer';
import './styles/globals.scss';
import { useState } from 'react';
import Detail from './pages/Detail';

function App() {
  const [page, setPage] = useState('landing');

  function changePage(page) {
    setPage(page);
  }

  return (
    <div className='root'>
      <Navbar changePage={changePage} />
      {page === 'landing' ? (
        <Landing changePage={changePage} />
      ) : page === 'home' ? (
        <Home changePage={changePage} />
      ) : (
        <Detail changePage={changePage} id={page} />
      )}

      <Footer />
    </div>
  );
}

export default App;
