import { apiKey, baseUrl, dbUrl } from './globals';

export async function fetchInfo(path, options) {
  return await fetch(`${baseUrl}${path}?key=${apiKey}&${options || ''}`);
}

export async function commentsHandler(id, method) {
  if (method === 'GET') {
    return await fetch(`${dbUrl}/comments?gameId=${id}`);
  }
  if (method === 'POST') {
    return async (body) => {
      const opts = {
        method: 'POST',
        body: JSON.stringify(body),
        headers: { 'Content-type': 'application/json; charseft=UFT-8' },
      };
      return await fetch(`${dbUrl}/comments`, opts);
    };
  }
}
