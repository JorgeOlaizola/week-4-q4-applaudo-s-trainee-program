# Videogames App - Week 4 Q4 Applaudo Trainee Program

Application that uses the RAWG.io api (https://api.rawg.io/docs/) to fetch videogames and the JSON-server api (https://github.com/typicode/json-server) to fetch comments for those videogames.

## Try it

Link to deploy: https://week-4-q4-applaudo-s-trainee-program.vercel.app/

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/JorgeOlaizola/week-4-q4-applaudo-s-trainee-program.git
```

Once the project it´s cloned, install dependencies

```bash
  npm install
```

In /src/adapters/globals.js change the const dbUrl to 'http://localhost:3004'

```javascript
...
  export const dbUrl = 'http://localhost:3004'
...
```

Before you run the project, execute the following command for 
starting the server.

```bash
  json-server --watch db.json --port 3004
```

In a separate bash console, execute the following command

```bash
  npm start
```
